#include "HoltMethod.h"

void HoltForecast(struct HoltForecastResult* result, double* data, int dataLength, int forecastLength, double alpha, double beta)
{
	result->Alpha = alpha;
	result->Beta = beta;
	result->InputLength = dataLength;
	result->ForecastLength = forecastLength;
	result->F = malloc((dataLength - 1) * sizeof(double));
	result->S = malloc((dataLength - 1) * sizeof(double));
	result->Forecast = malloc(forecastLength * sizeof(double));
	result->Smooth = malloc((dataLength - 2) * sizeof(double));
	result->Errors = malloc((dataLength - 2) * sizeof(double));

	double *F = result->F;
	*result->F = data[1];
	double *S = result->S;
	*result->S = data[1] - data[0];
	*result->Smooth = *result->F + *result->S;
	*result->Errors = data[2] - *result->Smooth;
	*result->Errors = *result->Errors * *result->Errors;
	result->RMSE = *result->Errors;

	double *Smooth = result->Smooth;
	double *Errors = result->Errors;

	double previousF = *F;
	double previousS = *S;
	result->F++;
	result->S++;
	result->Smooth++;
	result->Errors++;

	for (int i = 2; i < dataLength; i++, previousF = *result->F, previousS = *result->S, result->F++, result->S++, result->Smooth++, result->Errors++)
	{
		*result->F = alpha * data[i] + (1 - alpha) * (previousF + previousS);
		*result->S = beta * (*result->F - previousF) + (1 - beta) * previousS;
		*result->Smooth = *result->F + *result->S;
		if (i < dataLength - 1)
		{
			*result->Errors = (data[i + 1] - *result->Smooth);
			*result->Errors = *result->Errors * *result->Errors;
			result->RMSE += *result->Errors;
		}
	}
	result->RMSE = sqrt(result->RMSE / dataLength);
	result->F--;
	result->S--;
	for (int i = 0; i < forecastLength; i++)
	{
		result->Forecast[i] = *result->F + (i + 1) * *result->S;
	}
	result->F = F;
	result->S = S;
	result->Smooth = Smooth;
	result->Errors = Errors;
}