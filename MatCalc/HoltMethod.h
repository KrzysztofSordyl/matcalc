#pragma once

#include <stdlib.h>
#include <math.h>

struct HoltForecastResult
{
#pragma region parameters and scalars
	int InputLength;

	int ForecastLength;

	double Alpha;

	double Beta;

	double RMSE;
#pragma endregion

#pragma region data
	double* F;

	double* S;

	double* Smooth;

	double* Forecast;

	double* Errors;
#pragma endregion
};

void HoltForecast(struct HoltForecastResult* result, double data[], int dataLength, int forecastLength, double alpha, double beta);
