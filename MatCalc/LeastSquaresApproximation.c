#include "LeastSquaresApproximation.h"

struct Matrix* LsaApproximate(struct Matrix* x, struct Matrix* y)
{
	if (x->Height != y->Height)
		return NULL;
	return MatrixMultiply(
		MatrixInverse(
			MatrixMultiply(
				MatrixTranspose(x),
				x
			)
		),
		MatrixMultiply(
			MatrixTranspose(x),
			y
		)
	);
}