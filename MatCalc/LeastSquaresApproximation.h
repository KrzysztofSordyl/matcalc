#pragma once
#include <stdlib.h>
#include "Matrix.h"

struct Matrix* LsaApproximate(struct Matrix* x, struct Matrix* y);