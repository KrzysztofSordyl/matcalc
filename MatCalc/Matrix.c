#include "Matrix.h"

struct Matrix* Matrix_1(int h, int w)
{
	struct Matrix* m = malloc(sizeof(struct Matrix));
	m->Height = h;
	m->Width = w;
	m->Value = malloc(sizeof(double*) * h);
	for (int i = 0; i < h; i++)
	{
		m->Value[i] = malloc(sizeof(double) * w);
	}
	return m;
}

struct Matrix* Matrix_2(int h, int w, int minValue, int maxValue)
{
	struct Matrix* m = Matrix_1(h, w);
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			m->Value[i][j] = rand() % (maxValue - minValue) + minValue;
		}
	}
	return m;
}

void MatrixShow(struct Matrix * m, int cellWidth, int precision)
{
	char* format = malloc(sizeof(char) * 7);
	sprintf(format, "%%%d.%df", cellWidth, precision);
	strcat(format, "%");
	for (int i = 0; i < m->Height; i++)
	{
		for (int j = 0; j < m->Width; j++)
		{
			printf(format, m->Value[i][j]);
		}
		printf("\n");
	}
}

struct Matrix* MatrixTranspose(struct Matrix* m)
{
	struct Matrix* result = Matrix_1(m->Width, m->Height);
	for (int h = 0; h < m->Height; h++)
	{
		for (int w = 0; w < m->Width; w++)
		{
			result->Value[w][h] = m->Value[h][w];
		}
	}
	return result;
}

struct Matrix * MatrixAdd(struct Matrix * m1, struct Matrix * m2)
{
	if (m1->Height != m2->Height ||
		m1->Width != m2->Width)
		return NULL;

	struct Matrix* result = Matrix_1(m1->Height, m1->Width);
	for (int i = 0; i < m1->Height; i++)
	{
		for (int j = 0; j < m1->Width; j++)
		{
			result->Value[i][j] = m1->Value[i][j] + m2->Value[i][j];
		}
	}
	return result;
}

struct Matrix * MatrixSubtract(struct Matrix * m1, struct Matrix * m2)
{
	if (m1->Height != m2->Height ||
		m1->Width != m2->Width)
		return NULL;

	struct Matrix* result = Matrix_1(m1->Height, m1->Width);
	for (int i = 0; i < m1->Height; i++)
	{
		for (int j = 0; j < m1->Width; j++)
		{
			result->Value[i][j] = m1->Value[i][j] - m2->Value[i][j];
		}
	}
	return result;
}

struct Matrix * MatrixMultiply(struct Matrix * m1, struct Matrix * m2)
{
	if (m1->Width != m2->Height)
		return NULL;
	int w = m2->Width;
	int h = m1->Height;
	int length = m1->Width;
	struct Matrix* result = Matrix_1(h, w);
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			double value = 0;
			for (int k = 0; k < length; k++)
			{
				value += m1->Value[i][k] * m2->Value[k][j];
			}
			result->Value[i][j] = value;
		}
	}
	return result;
}

void parseRow(char * data, int line, struct Matrix * destination)
{
	if (line == 0)
	{
		sscanf(data, "%d %d", &destination->Height, &destination->Width);
		destination->Value = malloc(sizeof(double*) * destination->Height);
		for (int i = 0; i < destination->Height; i++)
		{
			destination->Value[i] = malloc(sizeof(double) * destination->Width);
		}
	}
	else
	{
		char* digit = strtok(data, " ");
		for (int i = 0; i < destination->Width; i++)
		{
			destination->Value[line - 1][i] = atof(digit);
			digit = strtok(NULL, " ");
		}
	}
}
struct Matrix * MatrixOpen(char * filename)
{
	struct Matrix* m = malloc(sizeof(struct Matrix));
	if (ReadTxt(filename, parseRow, m) == 0)
	{
		free(m);
		m = NULL;
	}
	return m;
}

char* provideRow(int line, struct Matrix * source)
{
	char* data = malloc(1024);
	data[0] = NULL;
	if (line == 0)
		sprintf(data, "%d %d", source->Height, source->Width);
	else
	{
		if (line > source->Height)
			return NULL;
		for (int i = 0; i < source->Width; i++)
		{
			char* cell = malloc(20);
			sprintf(cell, "%f ", source->Value[line - 1][i]);
			strcat(data, cell);
		}
	}
	return data;
}
int MatrixSave(char * filename, struct Matrix * m)
{
	return WriteTxt(filename, provideRow, m);
}

double* MatrixElementsAlgebraicComplement(struct Matrix* m, int x, int y)
{
	if (m->Width != m->Height)
		return NULL;
	double* complement = malloc(sizeof(double));
	*complement = 0;
	struct Matrix* m2 = Matrix_1(m->Height - 1, m->Width - 1);
	for (int h = 0; h < m->Height; h++)
	{
		if (h == y)
			continue;
		int destinationH = h - (h > y ? 1 : 0);
		for (int w = 0; w < m->Width; w++)
		{
			if (w == x)
				continue;
			int destinationW = w - (w > x ? 1 : 0);
			m2->Value[destinationH][destinationW] = m->Value[h][w];
		}
	}
	*complement = (((y + x + 2) % 2) == 1 ? -1 : 1) * *MatrixDeterminant(m2);
	return complement;
}

double* MatrixDeterminant(struct Matrix * m)
{
	if (m->Width != m->Height)
		return NULL;
	double* det = malloc(sizeof(double));
	if (m->Height == 1)
		*det = m->Value[0][0];
	else
	{
		*det = 0;
		for (int i = 0; i < m->Height; i++)
		{
			*det += *MatrixElementsAlgebraicComplement(m, 0, i) * m->Value[i][0];
		}
	}
	return det;
}

struct Matrix* MatrixAlgebraicComplement(struct Matrix* m)
{
	if (m->Width != m->Height)
		return NULL;
	struct Matrix* result = Matrix_1(m->Height, m->Width);
	for (int h = 0; h < m->Height; h++)
	{
		for (int w = 0; w < m->Width; w++)
		{
			result->Value[h][w] = *MatrixElementsAlgebraicComplement(m, w, h);
		}
	}
	return result;
}

struct Matrix * MatrixInverse(struct Matrix * m)
{
	if (m->Width != m->Height)
		return NULL;
	double* det = MatrixDeterminant(m);
	if (*det == 0)
		return NULL;
	struct Matrix* dt = MatrixTranspose(MatrixAlgebraicComplement(m));
	struct Matrix* result = Matrix_1(m->Height, m->Width);
	for (int h = 0; h < m->Height; h++)
	{
		for (int w = 0; w < m->Width; w++)
		{
			result->Value[h][w] = dt->Value[h][w] / *det;
		}
	}
	return result;
}

struct matrixRowParseDestination
{
	struct List* Matrix;

	int Header;

	int ColumnAmount;

	struct List* Columns;

	int CurrentColumn;
};
struct matrixRowParseDestination* matrixRowParseDestination(int header, struct List* columns)
{
	struct matrixRowParseDestination* result = malloc(sizeof(struct matrixRowParseDestination));
	result->Matrix = List();
	result->Header = header;
	result->ColumnAmount = -1;
	result->Columns = columns;
	result->CurrentColumn = 0;
	return result;
}

struct matrixElementParseDestination
{
	double* Vector;

	struct List* Columns;

	int CurrentColumn;
};
struct matrixElementParseDestination* matrixElementParseDestination(int length, struct List* columns)
{
	struct matrixElementParseDestination* result = malloc(sizeof(struct matrixElementParseDestination));
	result->Vector = malloc(sizeof(double) * length);
	result->Columns = columns;
	result->CurrentColumn = 0;
	return result;
}

int matrixElementParse(char* current, int index, struct matrixElementParseDestination* destination)
{
	if (ListContains(destination->Columns, &index))
	{
		destination->Vector[destination->CurrentColumn] = atof(current);
		destination->CurrentColumn++;
	}
	return 1;
}
int matrixRowParse(struct List* data, int line, struct matrixRowParseDestination *destination)
{
	if (line == 0 && destination->Header)
		return 1;
	if (destination->ColumnAmount == -1)
		destination->ColumnAmount = data->Count;
	else if (destination->ColumnAmount < data->Count)
		return 0;

	struct matrixElementParseDestination* row = matrixElementParseDestination(destination->ColumnAmount, destination->Columns);
	ListIterate(data, matrixElementParse, row);
	ListAdd_1(destination->Matrix, row->Vector);
	return 1;
}

int rowListElementParse(double* current, int index, struct Matrix* destination)
{
	destination->Value[index] = current;
	return 1;
}
struct Matrix* MatrixOpenCsv(char* filename, int header, char separator, struct List* columns)
{
	columns->Comparer = CompareIntegers;
	struct matrixRowParseDestination* destination = matrixRowParseDestination(header, columns);
	if (!ReadCsv_3(filename, separator, matrixRowParse, destination))
		return NULL;

	struct Matrix* iteratorDestination = Matrix_1(destination->Matrix->Count, columns->Count);
	ListIterate(destination->Matrix, rowListElementParse, iteratorDestination);
	return iteratorDestination;
}