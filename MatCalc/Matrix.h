#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Tools/TxtReader.h"
#include "Tools/CsvReader.h"
#include "Tools/MathHelper.h"

struct Matrix
{
	double** Value;

	int Width;

	int Height;
};

struct Matrix* Matrix_1(int h, int w);

struct Matrix* Matrix_2(int h, int w, int minValue, int maxValue);

void MatrixShow(struct Matrix* m, int cellWidth, int precision);

struct Matrix* MatrixTranspose(struct Matrix* m);

struct Matrix* MatrixAdd(struct Matrix* m1, struct Matrix* m2);

struct Matrix* MatrixSubtract(struct Matrix* m1, struct Matrix* m2);

struct Matrix* MatrixMultiply(struct Matrix* m1, struct Matrix* m2);

struct Matrix* MatrixOpen(char* filename);

int MatrixSave(char* filename, struct Matrix* m);

double* MatrixElementsAlgebraicComplement(struct Matrix* m, int x, int y);

double* MatrixDeterminant(struct Matrix* m);

struct Matrix* MatrixAlgebraicComplement(struct Matrix* m);

struct Matrix* MatrixInverse(struct Matrix* m);

struct Matrix* MatrixOpenCsv(char* filename, int header, char separator, struct List* columns);