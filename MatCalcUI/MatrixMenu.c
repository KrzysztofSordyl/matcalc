#include "MatrixMenu.h"

struct Dictionary* matrices;
struct Dictionary* commands;

void initialize();

void MatrixMenuRun()
{
	initialize();
	MatrixMenu_Help();
	while (1)
	{
		MatrixMenu_WaitForCommand();
	}
}

void MatrixMenu_Help()
{
	printf(
		"Commands:\n"
		"help | Shows this helper\n"
		"create <name> <height> <width> <min> <max> | Creates matrix\n"
		"add <result> <operand1> <operand2> | Adds matrices\n"
		"sub <result> <operand1> <operand2> | Subtracts matrices\n"
		"mul <result> <operand1> <operand2> | Multiplies matrices\n"
		"open <name> <filename> | Opens file with matrix\n"
		"save <name> <filename> | Saves matrix to file\n"
		"show <name> <cellWidth> <precision> | Shows matrix\n"
		"list | Lists all matrices\n");
}

void MatrixMenu_WaitForCommand()
{
	char* command = malloc(sizeof(char) * 10);
	char** params = malloc(sizeof(char*) * 5);
	for (int i = 0; i < 5; i++)
	{
		params[i] = malloc(sizeof(char) * 50);
	}
	char* input = malloc(sizeof(char) * 256);
	fgets(input, 256, stdin);
	char* s = strtok(input, " \n");
	command = s;
	for (int i = 0; i < 5; i++)
	{
		s = strtok(NULL, " \n");
		params[i] = s;
	}

	void* c = DictionaryTranslate(commands, command);
	if (c == NULL)
	{
		printf("Unrecognized command name.\n");
		return;
	}
	((void(*)())c)(params[0], params[1], params[2], params[3], params[4]);
}

void create(char * name, char * height, char * width, char * min, char * max)
{
	MatrixMenu_Create(name, atoi(height), atoi(width), atoi(min), atoi(max));
}
void addToDictionary(struct Matrix* m, char* name)
{
	int removed = DictionaryRemove(matrices, name);
	DictionaryAdd(matrices, name, m);
	if (removed)
		printf("Matrix %s replaced.\n", name);
	else
		printf("Matrix %s created.\n", name);
}
void MatrixMenu_Create(char * name, int height, int width, int min, int max)
{
	addToDictionary(Matrix_2(height, width, min, max), name);
}

void MatrixMenu_Add(char * result, char  * operand1, char  * operand2)
{
	struct Matrix* o1 = DictionaryTranslate(matrices, operand1);
	struct Matrix* o2 = DictionaryTranslate(matrices, operand2);
	int error = 0;
	if (o1 == NULL)
	{
		printf("First operand does not exist.\n");
		error = 1;
	}
	if (o2 == NULL)
	{
		printf("Second operand does not exist.\n");
		error = 1;
	}
	if (error)
		return; 
	struct Matrix* m = MatrixAdd(o1, o2);
	if (m == NULL)
	{
		printf("Matrices can not be added.\n");
		return;
	}
	addToDictionary(m, result);
}

void MatrixMenu_Subtract(char * result, char  * operand1, char  * operand2)
{
	struct Matrix* o1 = DictionaryTranslate(matrices, operand1);
	struct Matrix* o2 = DictionaryTranslate(matrices, operand2);
	int error = 0;
	if (o1 == NULL)
	{
		printf("First operand does not exist.\n");
		error = 1;
	}
	if (o2 == NULL)
	{
		printf("Second operand does not exist.\n");
		error = 1;
	}
	if (error)
		return;
	struct Matrix* m = MatrixSubtract(o1, o2);
	if (m == NULL)
	{
		printf("Matrices can not be subtracted.\n");
		return;
	}
	addToDictionary(m, result);
}

void MatrixMenu_Multiply(char * result, char  * operand1, char  * operand2)
{
	struct Matrix* o1 = DictionaryTranslate(matrices, operand1);
	struct Matrix* o2 = DictionaryTranslate(matrices, operand2);
	int error = 0;
	if (o1 == NULL)
	{
		printf("First operand does not exist.\n");
		error = 1;
	}
	if (o2 == NULL)
	{
		printf("Second operand does not exist.\n");
		error = 1;
	}
	if (error)
		return;
	struct Matrix* m = MatrixMultiply(o1, o2);
	if (m == NULL)
	{
		printf("Matrices can not be multplied.\n");
		return;
	}
	addToDictionary(m, result);
}

void MatrixMenu_Open(char * name, char * filename)
{
	struct Matrix* m = MatrixOpen(filename);
	if (m == NULL)
	{
		printf("File can not be opened.\n");
		return;
	}
	addToDictionary(m, name);
}

void MatrixMenu_Save(char * name, char * filename)
{
	struct Matrix* m = DictionaryTranslate(matrices, name);
	if (m == NULL)
	{
		printf("Matrix %s does not exist.\n", name);
		return;
	}
	if (MatrixSave(filename, m) == 0)
		printf("Matrix %s can not be saved.\n", name);
	else
		printf("Matrix %s has been saved successfully.\n", name);
}

void show(char * name, char * cellWidth, char * precision)
{
	MatrixMenu_Show(name, atoi(cellWidth), atoi(precision));
}
void MatrixMenu_Show(char * name, int cellWidth, int precision)
{
	struct Matrix* m = DictionaryTranslate(matrices, name);
	if (m == NULL)
	{
		printf("Matrix %s does not exist.\n", name);
		return;
	}
	MatrixShow(m, cellWidth, precision);
}

void printMatrix(char* name, struct Matrix* m, int index)
{
	printf("%10s%10d%10d\n", name, m->Height, m->Width);
}
void MatrixMenu_ListMatrices()
{
	printf("%10s%10s%10s\n", "Name", "Height", "Width");
	DictionaryForeach(matrices, printMatrix);
}

void initialize()
{
	matrices = Dictionary();
	matrices->Comparator = strcmp;
	commands = Dictionary();
	commands->Comparator = strcmp;
	DictionaryAdd(commands, "help", MatrixMenu_Help);
	DictionaryAdd(commands, "create", create);
	DictionaryAdd(commands, "add", MatrixMenu_Add);
	DictionaryAdd(commands, "sub", MatrixMenu_Subtract);
	DictionaryAdd(commands, "mul", MatrixMenu_Multiply);
	DictionaryAdd(commands, "open", MatrixMenu_Open);
	DictionaryAdd(commands, "save", MatrixMenu_Save);
	DictionaryAdd(commands, "show", show);
	DictionaryAdd(commands, "list", MatrixMenu_ListMatrices);
}