#pragma once
#include <stdlib.h>
#include <stdio.h>
#include "Tools/Dictionary.h"
#include "MatCalc/Matrix.h"

void MatrixMenuRun();

void MatrixMenu_Help();

void MatrixMenu_WaitForCommand();

void MatrixMenu_Create(char* name, int height, int width, int min, int max);

void MatrixMenu_Add(char * result, char * operand1, char * operand2);

void MatrixMenu_Subtract(char * result, char * operand1, char * operand2);

void MatrixMenu_Multiply(char * result, char * operand1, char * operand2);

void MatrixMenu_Open(char * name, char * filename);

void MatrixMenu_Save(char * name, char * filename);

void MatrixMenu_Show(char * name, int cellWidth, int precision);

void MatrixMenu_ListMatrices();