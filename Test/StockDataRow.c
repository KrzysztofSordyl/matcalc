#include "StockDataRow.h"

void StockDataRowParse(char ** data, char ** columns, struct StockDataRow* destination)
{
	for (int i = 0;; i++)
	{
		char* c = columns[i];
		char* d = data[i];
		if (c == NULL || d == NULL)
			break;

		if (!strcmp(c, "Data"))
			sscanf(d, "%d-%d-%d", &destination->Date.tm_year, &destination->Date.tm_mon, &destination->Date.tm_mday);
		else
			*((double*)StockDataRowGetValue(destination, c)) = atof(d);
			/*if (!strcmp(c, "Otwarcie"))
			destination->Open = atof(d);
		else if (!strcmp(c, "Zamkniecie"))
			destination->Close = atof(d);
		else if (!strcmp(c, "Najwyzszy"))
			destination->Max = atof(d);
		else if (!strcmp(c, "Najnizszy"))
			destination->Min = atof(d);*/
	}
}

void* StockDataRowGetValue(struct StockDataRow* source, char* field)
{
	if (!strcmp(field, "Data"))
		return &(source->Date);
	else if (!strcmp(field, "Otwarcie"))
		return &(source->Open);
	else if (!strcmp(field, "Zamkniecie"))
		return &(source->Close);
	else if (!strcmp(field, "Najwyzszy"))
		return &(source->Max);
	else if (!strcmp(field, "Najnizszy"))
		return &(source->Min);
}