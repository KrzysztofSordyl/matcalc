#pragma once

#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct StockDataRow
{
	struct tm Date;

	double Open;

	double Close;

	double Max;

	double Min;
};

void StockDataRowParse(char ** data, char ** columns, struct StockDataRow * destination);

void * StockDataRowGetValue(struct StockDataRow * source, char * field);
