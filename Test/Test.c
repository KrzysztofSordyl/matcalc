#include "Test.h"

int main()
{
	srand(time(NULL));
	//task1();
	//task2_1();
	//task2_2();

	struct List* columns = List();
	int i[] = {1, 2, 3, 4};
	ListAdd_1(columns, &i[0]);
	struct Matrix* y = MatrixOpenCsv("C:\\Users\\Krzysztof\\Desktop\\agh\\mag\\1 sem\\analiza\\proj\\1\\wig_banki_d.csv", 1, ';', columns);
	ListAdd_1(columns, &i[1]);
	ListAdd_1(columns, &i[2]);
	ListAdd_1(columns, &i[3]);
	struct Matrix* x = MatrixOpenCsv("C:\\Users\\Krzysztof\\Desktop\\agh\\mag\\1 sem\\analiza\\proj\\1\\wig_d.csv", 1, ';', columns);

	MatrixShow(LsaApproximate(x, y), 6, 3);
	system("pause");
	return 0;
}

void task1()
{
	// to kiedyś był alternatywny sposób wczytywania danych. Nie wiadomo czy jeszcze działa
	/*char*** csv = ReadCsv_1("D:\\MyPrograms\\AGH\\MiTP\\MatCalc\\eurpln_d.csv", 5, 50);
	char** columns = csv[0];
	struct StockDataRow data[49];
	for (int i = 0; i < 49; i++)
	{
	StockDataRowParse(csv[i + 1], columns, &data[i]);
	}*/

	// to może być rowinięte aby czytanie było elastyczne - samo wykrywało ilość kolumn i wierszy
	struct StockDataRow* stockData[560] = { malloc(sizeof(struct StockDataRow)) };
	char* columns[5];
	ReadCsv_2("D:\\MyPrograms\\AGH\\MiTP\\MatCalc\\eurpln_d.csv", 5, 560, StockDataRowParse, sizeof(struct StockDataRow), stockData, columns);

	printf("Holt's linear trend method\nChose one column:\n");
	for (int i = 0; i < 5; i++)
	{
		printf("%d - %s\n", i + 1, columns[i]);
	}
	int chosenColumn = -1;
	do
	{
		scanf("%d", &chosenColumn);
	} while (chosenColumn < 1 || chosenColumn > 5);

	double data[560];
	for (int i = 0; i < 560; i++)
	{
		data[i] = *(double*)StockDataRowGetValue(stockData[i], columns[chosenColumn - 1]);
	}
	// to w wypadku czytania danych w sposób ogólny - niezależny od typu danych
	/*for (int i = 0; i < 560; i++)
	{
	data[i] = atof(csv[i][chosenColumn - 1]);
	}*/

	/*printf("Passs alpha parameter [0-1 commonly]:\n");
	double alpha = INFINITY;
	while (!scanf("%lf", &alpha));
	printf("Passs beta parameter [0-1 commonly]:\n");
	double beta = INFINITY;
	while (!scanf("%lf", &beta));

	struct HoltForecastResult forecast;
	HoltForecast(&forecast, data, 560, 1, alpha, beta);
	printf("Forecast:\n");
	for (int i = 0; i < forecast.ForecastLength; i++, forecast.Forecast++)
	{
	printf("%f\n", *forecast.Forecast);
	}
	printf("RMSE: %f\n", forecast.RMSE);*/

	time_t t;
	srand(&t);
	double* temp = &data;
	struct HoltForecastResult forecasts[560 - 5];
	struct HoltForecastResult* forecastTemp = &forecasts;
	for (int t = 0; t < 560 - 5; t++, temp++, forecastTemp++)
	{
		struct HoltForecastResult best;
		for (int i = 0; i < 10; i++)
		{
			struct HoltForecastResult forecast;
			HoltForecast(&forecast, temp, 5, 1, rand() / (double)RAND_MAX, rand() / (double)RAND_MAX);
			if (i == 0 || best.RMSE > forecast.RMSE)
				best = forecast;
		}
		printf("t =%4d: alpha = %6f, beta = %6f, Forecast = %f\n", t + 5, best.Alpha, best.Beta, best.Forecast[0]);
		*forecastTemp = best;
	}
}

void task2_1()
{
	printf("\n2.1\n");
	struct Matrix* a = Matrix_2(10, 10, 1, 6);
	printf("a\n");
	MatrixShow(a, 2, 0);
	struct Matrix* b = Matrix_2(10, 10, 1, 6);
	printf("b\n");
	MatrixShow(b, 2, 0);
	struct Matrix* c = MatrixAdd(a, b);
	printf("c\n");
	MatrixShow(c, 2, 0);
	struct Matrix* d = MatrixSubtract(a, b);
	printf("d\n");
	MatrixShow(d, 2, 0);
	struct Matrix* e = MatrixMultiply(a, b);
	printf("e\n");
	MatrixShow(e, 6, 0);
}

void task2_2()
{
	MatrixMenuRun();
}