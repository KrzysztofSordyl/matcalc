#pragma once

#include <stdio.h>
#include "Tools/CsvReader.h"
#include "StockDataRow.h"
#include "MatCalc/HoltMethod.h"
#include "MatCalc/Matrix.h"
#include "MatCalc/LeastSquaresApproximation.h"
#include "MatCalcUI/MatrixMenu.h"

int main();

void task1();

void task2_1();

void task2_2();