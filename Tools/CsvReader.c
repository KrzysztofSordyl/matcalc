#include "CsvReader.h"

const char** readRow(char* line, int amount)
{
	if (amount <= 0)
		return NULL;
	char** values = (char**)malloc(sizeof(char*) * (amount + 1));
	values[amount] = NULL;

	const char* temp = strtok(line, ",");
	for (int i = 0;
		temp && *temp && i < amount;
		temp = strtok(NULL, ",\n"), i++)
	{
		values[i] = (char*)malloc(sizeof(char) * (strlen(temp) + 1));
		values[i][strlen(temp)] = NULL;
		strcpy(values[i], temp);
	}
	return values;
}

const char*** ReadCsv_1(char* filename, int colAmount, int rowAmount)
{
	FILE* stream = fopen(filename, "r");
	char*** csv = (char***)malloc(sizeof(char**) * (rowAmount + 2));
	csv[rowAmount + 1] = NULL;

	char line[1024];
	for (int i = 0;
		fgets(line, 1024, stream) && i < rowAmount + 1;
		i++)
	{
		char* tmp = _strdup(line);
		csv[i] = readRow(tmp, colAmount);
		free(tmp);
	}
	return csv;
}

void ReadCsv_2(char* filename, int colAmount, int rowAmount, void (*rowParse)(char** data, char** columns, void* destination), int t_size, void** destination, char** columns)
{
	FILE* stream = fopen(filename, "r");

	char line[1024];
	for (int i = 0;
		fgets(line, 1024, stream) && i < rowAmount + 1;
		i++)
	{
		char* tmp = _strdup(line);
		if (i == 0)
		{
			char** cols = readRow(tmp, colAmount);
			do
			{
				*columns = malloc(strlen(*cols) * sizeof(char));
				strcpy(*columns, *cols);
				columns++;
				cols++;
			} while (cols != NULL && *cols != NULL);
			columns -= colAmount;
		}
		else
		{
			destination[i - 1] = malloc(t_size);
			rowParse(readRow(tmp, colAmount), columns, destination[i - 1]);
		}
		free(tmp);
	}
}

struct parseRow3destination
{
	char Separator;

	int(*RowParse)(struct List* data, int line, void *destination);

	void* Destination;
};

struct parseRow3destination* parseRow3destination(char separator, int(*rowParse)(struct List* data, void *destination), void * destination)
{
	struct parseRow3destination* result = malloc(sizeof(struct parseRow3destination));
	result->Separator = separator;
	result->RowParse = rowParse;
	result->Destination = destination;
	return result;
}

int parseRow3(char* data, int line, struct parseRow3destination* destination)
{
	char* separators = malloc(sizeof(char) * 3);
	separators[0] = destination->Separator;
	separators[1] = '\n';
	separators[2] = NULL;

	struct List* dataList = List();
	char* value = strtok(data, separators);
	for (int i = 0; value && *value; value = strtok(NULL, separators), i++)
	{
		char* current = malloc(sizeof(char) * (strlen(value) + 1));
		current[strlen(value)] = NULL;
		strcpy(current, value);
		ListAdd_1(dataList, current);
	}
	return destination->RowParse(dataList, line, destination->Destination);
}
int ReadCsv_3(char * filename, char separator, int(*rowParse)(struct List* data, int line, void *destination), void * destination)
{
	return ReadTxt(filename, parseRow3, parseRow3destination(separator, rowParse, destination));
}
