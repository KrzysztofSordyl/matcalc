#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "TxtReader.h"
#include "List.h"

const char *** ReadCsv_1(char * filename, int colAmount, int rowAmount);

void ReadCsv_2(char * filename, int colAmount, int rowAmount, void(*rowParse)(char **data, char **columns, void *destination), int t_size, void ** destination, char** columns);

int ReadCsv_3(char * filename, char separator, int(*rowParse)(struct List* data, int line, void *destination), void * destination);