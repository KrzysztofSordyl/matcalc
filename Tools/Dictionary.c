#include "Dictionary.h"

struct KeyValuePair
{
	void* Key;

	void* Value;
};

struct KeyValuePair* KeyValuePair(void* key, void* value)
{
	struct KeyValuePair* pair = malloc(sizeof(struct KeyValuePair));
	pair->Key = key;
	pair->Value = value;
	return pair;
}

struct DictionaryContainsDestination
{
	int* Contains;

	void* Key;

	int(*Comparator)(void*, void*);
};

struct DictionaryContainsDestination* DictionaryContainsDestination(int* contains, void* key, int(*comparator)(void*, void*))
{
	struct DictionaryContainsDestination* destination = malloc(sizeof(struct DictionaryContainsDestination));
	destination->Contains = contains;
	destination->Key = key;
	destination->Comparator = comparator;
	return destination;
}

struct DictionaryRemoveDestination
{
	int* Removed;

	void* Key;

	struct List* List;

	int(*Comparator)(void*, void*);
};

struct DictionaryRemoveDestination* DictionaryRemoveDestination(int* removed, void* key, struct List* list, int(*comparator)(void*, void*))
{
	struct DictionaryRemoveDestination* destination = malloc(sizeof(struct DictionaryContainsDestination));
	destination->Removed = removed;
	destination->Key = key;
	destination->List = list;
	destination->Comparator = comparator;
	return destination;
}

struct DictionaryTranslateDestination
{
	void* Key;

	void* Value;

	int(*Comparator)(void*, void*);
};

struct DictionaryTranslateDestination* DictionaryTranslateDestination(void* key, void* value, int(*comparator)(void*, void*))
{
	struct DictionaryTranslateDestination* destination = malloc(sizeof(struct DictionaryTranslateDestination));
	destination->Key = key;
	destination->Value = value;
	destination->Comparator = comparator;
	return destination;
}

struct Dictionary* Dictionary()
{
	struct Dictionary* d = malloc(sizeof(struct Dictionary));
	d->List = List();
	d->Comparator = NULL;
	return d;
}

int DictionaryAdd(struct Dictionary* self, void* key, void* value)
{
	if (DictionaryContains(self, key) == 1)
		return 0;
	struct KeyValuePair* pair = KeyValuePair(key, value);
	ListAdd_1(self->List, pair);
	return 1;
}

int dictionaryRemoveIterator(struct Node* node, int index, struct DictionaryRemoveDestination* destination)
{
	struct KeyValuePair* pair = node->Value;
	if ((destination->Comparator != NULL &&
		destination->Comparator(pair->Key, destination->Key) == 0) ||
		pair->Key == destination->Key)
	{
		*destination->Removed = 1;
		ListRemove(destination->List, node);
		return 0;
	}
	return 1;
}
int DictionaryRemove(struct Dictionary* self, void* key)
{
	int removed = 0;
	struct DictionaryRemoveDestination* destination = DictionaryRemoveDestination(&removed, key, self->List, self->Comparator);
	ListIterateNodes(self->List, dictionaryRemoveIterator, destination);
	return removed;
}

int dictionaryContainsIterator(struct KeyValuePair* pair, int index, struct DictionaryContainsDestination* destination)
{
	if ((destination->Comparator != NULL &&
		destination->Comparator(pair->Key, destination->Key) == 0) ||
		pair->Key == destination->Key)
	{
		*destination->Contains = 1;
		return 0;
	}
	return 1;
}
int DictionaryContains(struct Dictionary* self, void* key)
{
	int contains = 0;
	struct DictionaryContainsDestination* destination = DictionaryContainsDestination(&contains, key, self->Comparator);
	ListIterate(self->List, dictionaryContainsIterator, destination);
	return contains;
}

int dictionaryTranslateIterator(struct KeyValuePair* pair, int index, struct DictionaryTranslateDestination* destination)
{
	if ((destination->Comparator != NULL &&
		destination->Comparator(pair->Key, destination->Key) == 0) ||
		pair->Key == destination->Key)
	{
		destination->Value = pair->Value;
		return 0;
	}
	return 1;
}
void* DictionaryTranslate(struct Dictionary* self, void* key)
{
	struct DictionaryTranslateDestination* destination = DictionaryTranslateDestination(key, NULL, self->Comparator);
	ListIterate(self->List, dictionaryTranslateIterator, destination);
	return destination->Value;
}

int dictionaryForeachIterator(struct KeyValuePair* pair, int index, void(*action)(void* key, void* value, int index))
{
	action(pair->Key, pair->Value, index);
	return 1;
}
void DictionaryForeach(struct Dictionary* self, void(*action)(void* key, void* value))
{
	ListIterate(self->List, dictionaryForeachIterator, action);
}