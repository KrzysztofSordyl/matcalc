#pragma once
#include <stdlib.h>
#include "List.h"

struct Dictionary
{
	struct List* List;

	int(*Comparator)(void*, void*);
};

struct Dictionary* Dictionary();

int DictionaryAdd(struct Dictionary* self, void* key, void* value);

int DictionaryRemove(struct Dictionary* self, void* key);

int DictionaryContains(struct Dictionary* self, void* key);

void* DictionaryTranslate(struct Dictionary* self, void* key);

void DictionaryForeach(struct Dictionary* self, void(*action)(void* key, void* value));