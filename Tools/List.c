#include "List.h"

struct Node * Node(struct Node * previous, struct Node * next, void * value)
{
	struct Node* node = malloc(sizeof(struct Node));
	node->Value = value;
	NodeInsert(node, previous, next);
	return node;
}

void NodeInsert(struct Node * node, struct Node * previous, struct Node * next)
{
	node->Previous = previous;
	node->Next = next;
	if (previous != NULL)
		previous->Next = node;
	if (next != NULL)
		next->Previous = node;
	return node;
}

void NodeRemove(struct Node* node)
{
	struct Node* next = NULL;
	struct Node* previous = NULL;
	if (node->Previous != NULL)
	{
		node->Previous->Next = NULL;
		previous = node->Previous;
		node->Previous = NULL;
	}
	if (node->Next != NULL)
	{
		node->Next->Previous = NULL;
		next = node->Next;
		node->Next = NULL;
	}
	if (previous != NULL && next != NULL)
	{
		previous->Next = next;
		next->Previous = previous;
	}
}

struct List* List()
{
	struct List* l = malloc(sizeof(struct List));
	l->First = NULL;
	l->Last = NULL;
	l->Count = 0;
	return l;
}

void ListAdd_1(struct List* self, void* item)
{
	ListAdd_2(self, Node(NULL, NULL, item));
}

void ListAdd_2(struct List* self, struct Node* item)
{
	if (self->First == NULL)
		self->First = item;
	else
	{
		self->Last->Next = item;
		item->Previous = self->Last;
	}
	self->Last = item;
	self->Count++;
}

void ListRemove(struct List* self, struct Node* item)
{
	if (self->First == item)
		self->First = item->Next;
	if (self->Last == item)
		self->Last = item->Previous;
	NodeRemove(item);
	self->Count--;
}

void ListIterate(struct List* self, int(*iterator)(void* current, int index, void* destination), void* destination)
{
	struct Node* node = self->First;
	for (int i = 0; node != NULL; i++)
	{
		if (iterator(node->Value, i, destination) == 0)
			break;
		node = node->Next;
	}
}

void ListIterateNodes(struct List* self, int(*iterator)(struct Node* current, int index, void* destination), void* destination)
{
	struct Node* node = self->First;
	for (int i = 0; node != NULL; i++)
	{
		if (iterator(node, i, destination) == 0)
			break;
		node = node->Next;
	}
}

struct listContainsDestination
{
	int Contains;

	void* Searched;

	struct List* List;
};
struct listContainsDestination* listContainsDestination(void* searched, struct List* list)
{
	struct listContainsDestination* result = malloc(sizeof(struct listContainsDestination));
	result->Contains = 0;
	result->Searched = searched;
	result->List = list;
	return result;
}

int listContainsIterator(void* current, int index, struct listContainsDestination* destination)
{
	if (destination->List->Comparer == NULL)
	{
		if (current == destination->Searched)
		{
			destination->Contains = 1;
			return 0;
		}
	}
	else
	{
		if (destination->List->Comparer(current, destination->Searched) == 0)
		{
			destination->Contains = 1;
			return 0;
		}
	}
	return 1;
}
int ListContains(struct List* self, void* item)
{
	struct listContainsDestination* destination = listContainsDestination(item, self);
	ListIterate(self, listContainsIterator, destination);
	return destination->Contains;
}