#pragma once
#include <stdlib.h>

struct Node
{
	struct Node* Previous;

	struct Node* Next;

	void* Value;
};

struct Node* Node(struct Node* previous, struct Node* next, void* value);

void NodeInsert(struct Node* node, struct Node* previous, struct Node* next);

void NodeRemove(struct Node* node);

struct List
{
	struct Node* First;

	struct Node* Last;

	int Count;

	int(*Comparer)(void* item1, void* item2);
};

struct List* List();

void ListAdd_1(struct List* self, void* item);

void ListAdd_2(struct List* self, struct Node* item);

void ListRemove(struct List* self, struct Node* item);

void ListIterate(struct List* self, int(*iterator)(void* current, int index, void* destination), void* destination);

void ListIterateNodes(struct List* self, int(*iterator)(struct Node* current, int index, void* destination), void* destination);

int ListContains(struct List* self, void* item);