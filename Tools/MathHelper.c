#include "MathHelper.h"

int IntParse(char* tekst, int* digit)
{
	return sscanf(tekst, "%d", digit);
}

int DoubleParse(char* tekst, double* digit)
{
	return sscanf(tekst, "%f", digit);
}

int CompareIntegers(int* i1, int* i2)
{
	return *i1 - *i2;
}