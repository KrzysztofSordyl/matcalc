#pragma once
#include <stdio.h>

int IntParse(char* tekst, int* digit);

int DoubleParse(char* tekst, double* digit);

int CompareIntegers(int* i1, int* i2);