#include "TxtReader.h"

int ReadTxt(char* filename, int(*parseRow)(char* data, int line, void* destination), void* destination)
{
	FILE* f = fopen(filename, "r");
	if (f == NULL)
		return 0;
	char line[1024];
	for (int i = 0; fgets(line, 1024, f); i++)
	{
		if (!parseRow(line, i, destination))
		{
			fclose(f);
			return 0;
		}
	}
	fclose(f);
	return 1;
}

int WriteTxt(char* filename, char* (*provideRow)(int line, void* source), void* source)
{
	FILE* f = fopen(filename, "w");
	if (f == NULL)
		return 0;
	for (int i = 0;; i++)
	{
		char* line = provideRow(i, source);
		if (line == NULL)
			break;
		strcat(line, "\n");
		fputs(line, f);
	}
	fclose(f);
	return 1;
}
