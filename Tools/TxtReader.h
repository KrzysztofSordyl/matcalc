#pragma once
#include <stdio.h>
#include <string.h>
#include "List.h"

int ReadTxt(char* filename, int(*parseRow)(char* data, int line, void* destination), void* destination);

int WriteTxt(char* filename, char* (*provideRow)(int line, void* source), void* source);